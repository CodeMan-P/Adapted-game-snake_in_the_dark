# 暗夜贪吃蛇

#### 介绍
 项目基于https://gitee.com/mvpred/snake修改。

 添加昼夜切换。

#### 核心功能

1. 定时切换昼夜
2. 空格加速

#### 效果预览
![输入图片说明](https://images.gitee.com/uploads/images/2020/0721/004914_a8516113_1633737.gif "b.gif")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0721/003515_2a131baf_1633737.png "批注 2020-07-21 002613.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0721/003531_b1b925bf_1633737.png "批注 2020-07-21 002634.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0721/003539_b908ba05_1633737.png "批注 2020-07-21 002659.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0721/003548_f144026c_1633737.png "批注 2020-07-21 002711.png")
#### 依赖

1.  pygame

#### LICENSE
MulanPSL-2.0